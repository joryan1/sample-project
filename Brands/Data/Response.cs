﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Brands.Data
{
    public class Response
    {
        [JsonProperty("brands")]
        public List<Brand> brands { get; set; }
    }
}
