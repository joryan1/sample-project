#pragma checksum "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "fb11048d48634e31f0999c667b777a8c86c5e5d9"
// <auto-generated/>
#pragma warning disable 1591
namespace Brands.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 2 "C:\Users\aaa\source\repos\Brands\Brands\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\aaa\source\repos\Brands\Brands\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\aaa\source\repos\Brands\Brands\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\aaa\source\repos\Brands\Brands\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\aaa\source\repos\Brands\Brands\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\aaa\source\repos\Brands\Brands\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\aaa\source\repos\Brands\Brands\_Imports.razor"
using Brands;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\aaa\source\repos\Brands\Brands\_Imports.razor"
using Brands.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\aaa\source\repos\Brands\Brands\_Imports.razor"
using Radzen;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\aaa\source\repos\Brands\Brands\_Imports.razor"
using Radzen.Blazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
using Brands.Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
using Newtonsoft.Json;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/brands")]
    public partial class BrandsComponent : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h3 style=\"margin-bottom: 2rem;\"><b>Administration</b></h3>\r\n\r\n");
            __builder.OpenElement(1, "div");
            __builder.AddAttribute(2, "style", "margin: 2rem;");
            __builder.AddMarkupContent(3, "\r\n    ");
            __builder.AddMarkupContent(4, "<h3 style=\"margin-bottom: 4rem \"><b>Brands</b></h3>\r\n    ");
            __builder.OpenElement(5, "div");
            __builder.AddAttribute(6, "style", "display: flex; justify-content: space-between;");
            __builder.AddMarkupContent(7, "\r\n        ");
            __builder.OpenElement(8, "div");
            __builder.AddMarkupContent(9, "\r\n            ");
            __builder.OpenElement(10, "button");
            __builder.AddAttribute(11, "type", "button");
            __builder.AddAttribute(12, "class", "btn btn-primary custom-btn");
            __builder.AddAttribute(13, "style", "display: flex;");
            __builder.AddAttribute(14, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 15 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                                                                                      AddNewBrand

#line default
#line hidden
#nullable disable
            ));
            __builder.AddMarkupContent(15, "\r\n                ");
            __builder.AddMarkupContent(16, "<span style=\"margin-right: 1rem;\" class=\"material-icons\">add_circle_outline</span> Add New Brand\r\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(17, "\r\n\r\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(18, "\r\n        ");
            __builder.OpenElement(19, "div");
            __builder.AddAttribute(20, "style", "display: flex; align-items: center; justify-content: center; margin-bottom: 2rem;");
            __builder.AddMarkupContent(21, "\r\n            ");
            __builder.AddMarkupContent(22, @"<div class=""input-group"" style=""margin-right:.8rem;"">
                <input type=""text"" placeholder=""Quick Search"" class=""form-control"" style=""border-right: none;"">
                <div class=""input-group-append"" style=""align-self: start;"">
                    <span class=""input-group-text"" style="" border-left: none; background-color: #fff0;"">
                        <span class=""material-icons"" style=""color: #C5C7CD;"">
                            search
                        </span>
                    </span>
                </div>
            </div>
            ");
            __builder.AddMarkupContent(23, "<button type=\"button\" class=\"btn btn-primary custom-btn\" style=\"margin-right: 0.8rem;\">SEARCH</button>\r\n            ");
            __builder.OpenElement(24, "button");
            __builder.AddAttribute(25, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 32 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                              loadBrands

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(26, "type", "button");
            __builder.AddAttribute(27, "class", "btn btn-primary custom-btn");
            __builder.AddContent(28, "REFRESH");
            __builder.CloseElement();
            __builder.AddMarkupContent(29, "\r\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(30, "\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(31, "\r\n\r\n");
#nullable restore
#line 36 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
     if (brands == null)
    {

#line default
#line hidden
#nullable disable
            __builder.AddContent(32, "        ");
            __builder.AddMarkupContent(33, "<div class=\"spinner-border m-4\" role=\"status\">\r\n            <span class=\"sr-only\">Loading...</span>\r\n        </div>\r\n");
#nullable restore
#line 41 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
    }
    else
    {

#line default
#line hidden
#nullable disable
            __builder.AddContent(34, "        ");
            __builder.OpenComponent<Radzen.Blazor.RadzenGrid<Brand>>(35);
            __builder.AddAttribute(36, "AllowFiltering", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 44 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                    true

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(37, "FilterCaseSensitivity", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Radzen.FilterCaseSensitivity>(
#nullable restore
#line 44 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                                                 FilterCaseSensitivity.CaseInsensitive

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(38, "AllowPaging", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 44 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                                                                                                     true

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(39, "PageSize", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Int32>(
#nullable restore
#line 44 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                                                                                                                     5

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(40, "AllowSorting", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 44 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                                                                                                                                      true

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(41, "Data", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Collections.Generic.IEnumerable<Brand>>(
#nullable restore
#line 44 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                                                                                                                                                   brands

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(42, "ColumnWidth", "200px");
            __builder.AddAttribute(43, "AllowAdd", "true");
            __builder.AddAttribute(44, "Columns", (Microsoft.AspNetCore.Components.RenderFragment)((__builder2) => {
                __builder2.AddMarkupContent(45, "\r\n                ");
                __builder2.OpenComponent<Radzen.Blazor.RadzenGridColumn<Brand>>(46);
                __builder2.AddAttribute(47, "Property", "id");
                __builder2.AddAttribute(48, "Title", "ID");
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(49, "\r\n                ");
                __builder2.OpenComponent<Radzen.Blazor.RadzenGridColumn<Brand>>(50);
                __builder2.AddAttribute(51, "Property", "code");
                __builder2.AddAttribute(52, "Title", "CODE");
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(53, "\r\n                ");
                __builder2.OpenComponent<Radzen.Blazor.RadzenGridColumn<Brand>>(54);
                __builder2.AddAttribute(55, "Property", "description");
                __builder2.AddAttribute(56, "Title", "DESCRIPTION");
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(57, "\r\n                ");
                __builder2.OpenComponent<Radzen.Blazor.RadzenGridColumn<Brand>>(58);
                __builder2.AddAttribute(59, "Property", "action");
                __builder2.AddAttribute(60, "Title", "Action");
                __builder2.AddAttribute(61, "Template", (Microsoft.AspNetCore.Components.RenderFragment<Brand>)((brand) => (__builder3) => {
                    __builder3.AddMarkupContent(62, "\r\n                        ");
                    __builder3.OpenComponent<Radzen.Blazor.RadzenButton>(63);
                    __builder3.AddAttribute(64, "Icon", "edit");
                    __builder3.AddAttribute(65, "Size", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Radzen.ButtonSize>(
#nullable restore
#line 51 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                                        ButtonSize.Small

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(66, "Style", "background-color: #fbbc05");
                    __builder3.AddAttribute(67, "Click", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Web.MouseEventArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 51 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                                                                                                     args => EditBrand(brand)

#line default
#line hidden
#nullable disable
                    )));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(68, "\r\n                    ");
                }
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(69, "\r\n            ");
            }
            ));
            __builder.CloseComponent();
            __builder.AddMarkupContent(70, "\r\n");
#nullable restore
#line 57 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
    }

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.AddMarkupContent(71, "\r\n\r\n\r\n\r\n    ");
            __builder.AddMarkupContent(72, @"<style>
        .custom-btn {
            padding: 0.4rem 0.8rem;
            height: auto;
            background-color: #2F44D3;
        }

        .brands-table th {
            text-align: center;
        }

        .brands-table td {
            padding: 0.2rem;
        }

        .brands-table th, .brands-table td {
            border: none !important;
            text-align: center;
        }

        .brands-table.table-striped tbody tr:nth-of-type(odd) {
            background-color: #edf1f2;
        }
    </style>");
        }
        #pragma warning restore 1998
#nullable restore
#line 59 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
       
    protected override void OnInitialized()
    {
        dialogService.OnOpen += Open;
        dialogService.OnClose += Close;
    }

    void Open(string title, Type type, Dictionary<string, object> parameters, DialogOptions options)
    {
        StateHasChanged();
    }

    void Close(dynamic result)
    {
        StateHasChanged();
        Status = false;
        brand.description = "";
        brand.code = "";
    }

    async Task AddNewBrand() => await dialogService.OpenAsync("Add New Brand", ds =>
    

#line default
#line hidden
#nullable disable
        (__builder2) => {
            __builder2.OpenComponent<Radzen.Blazor.RadzenTemplateForm<Brand>>(73);
            __builder2.AddAttribute(74, "Data", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Brand>(
#nullable restore
#line 80 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                              brand

#line default
#line hidden
#nullable disable
            ));
            __builder2.AddAttribute(75, "Submit", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Brand>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Brand>(this, 
#nullable restore
#line 80 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                                             AddBrand

#line default
#line hidden
#nullable disable
            )));
            __builder2.AddAttribute(76, "InvalidSubmit", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Radzen.FormInvalidSubmitEventArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Radzen.FormInvalidSubmitEventArgs>(this, 
#nullable restore
#line 80 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                                                                      OnInvalidSubmit

#line default
#line hidden
#nullable disable
            )));
            __builder2.AddAttribute(77, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Forms.EditContext>)((context) => (__builder3) => {
                __builder3.AddMarkupContent(78, "\r\n    ");
                __builder3.OpenElement(79, "div");
                __builder3.AddAttribute(80, "class", "container");
                __builder3.AddAttribute(81, "style", "padding: 1rem 4rem;");
                __builder3.AddMarkupContent(82, "\r\n");
#nullable restore
#line 82 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
         if (Status) {

#line default
#line hidden
#nullable disable
                __builder3.AddContent(83, "            ");
                __builder3.OpenElement(84, "div");
                __builder3.AddAttribute(85, "class", "alert" + " " + (
#nullable restore
#line 83 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                               StatusClass

#line default
#line hidden
#nullable disable
                ) + " mb-4");
                __builder3.AddAttribute(86, "role", "alert");
                __builder3.OpenElement(87, "small");
                __builder3.AddContent(88, 
#nullable restore
#line 83 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                                                      StatusMessage

#line default
#line hidden
#nullable disable
                );
                __builder3.CloseElement();
                __builder3.CloseElement();
                __builder3.AddMarkupContent(89, "\r\n");
#nullable restore
#line 84 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
        }

#line default
#line hidden
#nullable disable
                __builder3.AddMarkupContent(90, "        \r\n        ");
                __builder3.OpenElement(91, "div");
                __builder3.AddAttribute(92, "class", "form-group row");
                __builder3.AddAttribute(93, "style", "align-items: center; margin: 0 0 1rem;");
                __builder3.AddMarkupContent(94, "\r\n            ");
                __builder3.AddMarkupContent(95, "<label style=\"width: 7rem;\">Code</label>\r\n            ");
                __builder3.OpenElement(96, "div");
                __builder3.AddAttribute(97, "class", "col-sm");
                __builder3.AddMarkupContent(98, "\r\n                ");
                __builder3.OpenComponent<Radzen.Blazor.RadzenTextBox>(99);
                __builder3.AddAttribute(100, "style", "display: block; width: 100%;");
                __builder3.AddAttribute(101, "Name", "Code");
                __builder3.AddAttribute(102, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 89 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                                                                             brand.code

#line default
#line hidden
#nullable disable
                ));
                __builder3.AddAttribute(103, "ValueChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => brand.code = __value, brand.code))));
                __builder3.AddAttribute(104, "ValueExpression", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<System.String>>>(() => brand.code));
                __builder3.CloseComponent();
                __builder3.AddMarkupContent(105, "\r\n                ");
                __builder3.OpenComponent<Radzen.Blazor.RadzenRequiredValidator>(106);
                __builder3.AddAttribute(107, "Component", "Code");
                __builder3.AddAttribute(108, "Text", "code is required");
                __builder3.CloseComponent();
                __builder3.AddMarkupContent(109, "\r\n            ");
                __builder3.CloseElement();
                __builder3.AddMarkupContent(110, "\r\n        ");
                __builder3.CloseElement();
                __builder3.AddMarkupContent(111, "\r\n        ");
                __builder3.OpenElement(112, "div");
                __builder3.AddAttribute(113, "class", "form-group row");
                __builder3.AddAttribute(114, "style", "align-items: center; margin: 0 0 1rem;");
                __builder3.AddMarkupContent(115, "\r\n            ");
                __builder3.AddMarkupContent(116, "<label style=\"width: 7rem;\">Description</label>\r\n            ");
                __builder3.OpenElement(117, "div");
                __builder3.AddAttribute(118, "class", "col-sm");
                __builder3.AddMarkupContent(119, "\r\n                ");
                __builder3.OpenComponent<Radzen.Blazor.RadzenTextBox>(120);
                __builder3.AddAttribute(121, "style", "display: block; width: 100%;");
                __builder3.AddAttribute(122, "Name", "Description");
                __builder3.AddAttribute(123, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 96 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                                                                                    brand.description

#line default
#line hidden
#nullable disable
                ));
                __builder3.AddAttribute(124, "ValueChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => brand.description = __value, brand.description))));
                __builder3.AddAttribute(125, "ValueExpression", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<System.String>>>(() => brand.description));
                __builder3.CloseComponent();
                __builder3.AddMarkupContent(126, "\r\n                ");
                __builder3.OpenComponent<Radzen.Blazor.RadzenRequiredValidator>(127);
                __builder3.AddAttribute(128, "Component", "Description");
                __builder3.AddAttribute(129, "Text", "Description is required");
                __builder3.CloseComponent();
                __builder3.AddMarkupContent(130, "\r\n            ");
                __builder3.CloseElement();
                __builder3.AddMarkupContent(131, "\r\n        ");
                __builder3.CloseElement();
                __builder3.AddMarkupContent(132, "\r\n        ");
                __builder3.AddMarkupContent(133, @"<div class=""form-check"" style=""margin-left: 8rem; margin-top: 2rem;"">
            <input class=""form-check-input"" type=""checkbox"" value id=""defaultCheck1"">
            <label class=""form-check-label"" for=""defaultCheck1"">
                Inactive
            </label>
        </div>
        ");
                __builder3.OpenElement(134, "div");
                __builder3.AddAttribute(135, "class", "d-flex justify-content-end pt-4");
                __builder3.AddMarkupContent(136, "\r\n            ");
                __builder3.OpenComponent<Radzen.Blazor.RadzenButton>(137);
                __builder3.AddAttribute(138, "ButtonType", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Radzen.ButtonType>(
#nullable restore
#line 107 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                      ButtonType.Submit

#line default
#line hidden
#nullable disable
                ));
                __builder3.AddAttribute(139, "Text", "ADD");
                __builder3.AddAttribute(140, "Style", "margin-right: 0.8rem; height: 2.5rem; background-color: var(--primary)");
                __builder3.CloseComponent();
                __builder3.AddMarkupContent(141, "\r\n            ");
                __builder3.OpenElement(142, "button");
                __builder3.AddAttribute(143, "style", "width: 7rem;");
                __builder3.AddAttribute(144, "type", "button");
                __builder3.AddAttribute(145, "class", "btn btn-secondary");
                __builder3.AddAttribute(146, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 108 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                                                                           ()=> ds.Close(false)

#line default
#line hidden
#nullable disable
                ));
                __builder3.AddContent(147, "CANCEL");
                __builder3.CloseElement();
                __builder3.AddMarkupContent(148, "\r\n        ");
                __builder3.CloseElement();
                __builder3.AddMarkupContent(149, "\r\n    ");
                __builder3.CloseElement();
                __builder3.AddMarkupContent(150, "\r\n");
            }
            ));
            __builder2.CloseComponent();
        }
#nullable restore
#line 111 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                     , new DialogOptions() { Width = "700px" });

    async Task EditBrand(Brand brand) => await dialogService.OpenAsync("Edit Brand", ds =>
    

#line default
#line hidden
#nullable disable
        (__builder2) => {
            __builder2.OpenElement(151, "div");
            __builder2.AddAttribute(152, "class", "container");
            __builder2.AddAttribute(153, "style", "padding: 1rem 4rem;");
            __builder2.AddMarkupContent(154, "\r\n        ");
            __builder2.OpenElement(155, "p");
            __builder2.AddContent(156, "current id is ");
            __builder2.AddContent(157, 
#nullable restore
#line 115 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                          brand.id

#line default
#line hidden
#nullable disable
            );
            __builder2.CloseElement();
            __builder2.AddMarkupContent(158, "\r\n    ");
            __builder2.OpenElement(159, "div");
            __builder2.AddAttribute(160, "class", "form-group row");
            __builder2.AddAttribute(161, "style", "align-items: center;");
            __builder2.AddMarkupContent(162, "\r\n        ");
            __builder2.AddMarkupContent(163, "<label style=\"width: 7rem;\">Code</label>\r\n        ");
            __builder2.OpenElement(164, "div");
            __builder2.AddAttribute(165, "class", "col-sm");
            __builder2.AddMarkupContent(166, "\r\n            ");
            __builder2.OpenElement(167, "input");
            __builder2.AddAttribute(168, "type", "text");
            __builder2.AddAttribute(169, "class", "form-control");
            __builder2.AddAttribute(170, "value", 
#nullable restore
#line 119 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                                            brand.code

#line default
#line hidden
#nullable disable
            );
            __builder2.CloseElement();
            __builder2.AddMarkupContent(171, "\r\n        ");
            __builder2.CloseElement();
            __builder2.AddMarkupContent(172, "\r\n    ");
            __builder2.CloseElement();
            __builder2.AddMarkupContent(173, "\r\n    ");
            __builder2.OpenElement(174, "div");
            __builder2.AddAttribute(175, "class", "form-group row");
            __builder2.AddAttribute(176, "style", "align-items: center;");
            __builder2.AddMarkupContent(177, "\r\n        ");
            __builder2.AddMarkupContent(178, "<label style=\"width: 7rem;\">Description</label>\r\n        ");
            __builder2.OpenElement(179, "div");
            __builder2.AddAttribute(180, "class", "col-sm");
            __builder2.AddMarkupContent(181, "\r\n            ");
            __builder2.OpenElement(182, "input");
            __builder2.AddAttribute(183, "type", "text");
            __builder2.AddAttribute(184, "class", "form-control");
            __builder2.AddAttribute(185, "value", 
#nullable restore
#line 125 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                                            brand.description

#line default
#line hidden
#nullable disable
            );
            __builder2.CloseElement();
            __builder2.AddMarkupContent(186, "\r\n        ");
            __builder2.CloseElement();
            __builder2.AddMarkupContent(187, "\r\n    ");
            __builder2.CloseElement();
            __builder2.AddMarkupContent(188, "\r\n    ");
            __builder2.AddMarkupContent(189, "<div class=\"form-check\" style=\"margin-left: 7rem;\">\r\n        <input class=\"form-check-input\" type=\"checkbox\" value id=\"defaultCheck1\">\r\n        <label class=\"form-check-label\" for=\"defaultCheck1\">\r\n            Inactive\r\n        </label>\r\n    </div>\r\n    ");
            __builder2.OpenElement(190, "div");
            __builder2.AddAttribute(191, "class", "d-flex justify-content-end pt-4");
            __builder2.AddMarkupContent(192, "\r\n        ");
            __builder2.OpenElement(193, "button");
            __builder2.AddAttribute(194, "style", "width: 7rem; margin-right: 0.6rem;");
            __builder2.AddAttribute(195, "type", "button");
            __builder2.AddAttribute(196, "class", "btn btn-primary");
            __builder2.AddAttribute(197, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 135 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                                                                                           ()=> ds.Close(true)

#line default
#line hidden
#nullable disable
            ));
            __builder2.AddContent(198, "ADD");
            __builder2.CloseElement();
            __builder2.AddMarkupContent(199, "\r\n        ");
            __builder2.OpenElement(200, "button");
            __builder2.AddAttribute(201, "style", "width: 7rem;");
            __builder2.AddAttribute(202, "type", "button");
            __builder2.AddAttribute(203, "class", "btn btn-secondary");
            __builder2.AddAttribute(204, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 136 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
                                                                                       ()=> ds.Close(false)

#line default
#line hidden
#nullable disable
            ));
            __builder2.AddContent(205, "CANCEL");
            __builder2.CloseElement();
            __builder2.AddMarkupContent(206, "\r\n    ");
            __builder2.CloseElement();
            __builder2.AddMarkupContent(207, "\r\n");
            __builder2.CloseElement();
        }
#nullable restore
#line 138 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
      , new DialogOptions() { Width = "700px" });
    

#line default
#line hidden
#nullable disable
#nullable restore
#line 141 "C:\Users\aaa\source\repos\Brands\Brands\Pages\BrandsComponent.razor"
           
        IEnumerable<Brand> brands;
        bool brandLoaded;

        // Get Brands
        protected override async Task OnInitializedAsync()
        {
            await loadBrands();
        }

        private async Task loadBrands()
        {
            var apiName = "Admin/Brands/GetBrands?PageSize=99999";
            var httpResponse = await client.GetAsync(apiName);
            try
            {
                if (httpResponse.IsSuccessStatusCode)
                {
                    Response responseData = JsonConvert.DeserializeObject<Response>(await httpResponse.Content.ReadAsStringAsync());
                    brands = responseData.brands;
                    brandLoaded = true;
                }
            } catch(Exception e)
            {
                brandLoaded = false;
            }
        }

        // Add Brand

        private string StatusMessage;
        private string StatusClass;
        private bool Status = false;
        private Brand brand = new Brand();

        protected void OnInvalidSubmit(FormInvalidSubmitEventArgs args)
        {
            Status = true;
            StatusClass = "alert-danger";
            StatusMessage = "Please fill the required fields!";
        }

        public string CodeValue { get; set; }
        public string CodeDescription { get; set; }
        bool loadFailed;

        protected async Task AddBrand()
        {
            Status = true;
            try
            {
                loadFailed = false;
                await client.SendJsonAsync(HttpMethod.Post, "Admin/Brands/AddBrand", brand);
            } catch (Exception e)
            {
                loadFailed = true;
            }

            if (!loadFailed)
            {
                StatusClass = "alert-success";
                StatusMessage = "Brand added successfully!";
            } else
            {
                StatusClass = "alert-danger";
                StatusMessage = "Code already exists!";
            }
        }
    

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient client { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private DialogService dialogService { get; set; }
    }
}
#pragma warning restore 1591
